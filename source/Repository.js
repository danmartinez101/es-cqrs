export default class Repository {
  constructor(storage, Aggregate) {
    this._storage = storage;
    this._Aggregate = Aggregate;
  }

  save(aggregate, expectedVersion) {
    let id = aggregate.id;
    let changes = aggregate.getUncommittedChanges();
    return this._storage.saveEvents(id, changes, expectedVersion);
  }

  getById(id) {
    return this._storage.getEventsForAggregate(id)
      .then((events) => {
        var obj = new this._Aggregate();
        obj.loadsFromHistory(events);
        return obj;
      });
  }
}
