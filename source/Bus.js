import assert from 'assert';
import Q from 'q';

export default class Bus {

  constructor() {
    this.maps = {};
  }

  registerHandler(key, handler) {

    let maps = this.maps[key] || [];
    this.maps[key] = [
      ...maps,
      handler
    ];

  }

  send(command) {
    return Q.Promise((resolve) => {

      let key = command.messageType;
      let handlers = this.maps[key];

      assert(handlers, 'no handler registered');
      assert(handlers.length === 1, 'More than one command handler registered');

      let handler = handlers[0];
      return resolve(handler.handle(command));

    });

  }

  publish(event) {

    let key = event.messageType;

    let handlers = this.maps[key] || [];

    let allPromises = [];
    handlers.forEach(handler => {
      allPromises.push(handler.handle(event));
    });

    return Q.all(allPromises);

  }

}
