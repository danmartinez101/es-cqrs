export default class AggregateRoot {

  constructor() {
    this._changes = [];
    this.version = 0;
  }

  getUncommittedChanges() {
    return this._changes;
  }

  markChangesAsCommitted() {
    this._changes.length = 0;
  }

  loadsFromHistory(history) {
    history.forEach(event => {
      this.applyChange(event, false);
    });
  }

  applyChange(event, isNew = true) {
    this.apply(event);
    if (isNew) {
      this._changes.push(event);
    }
  }

  apply() {
    throw new Error('Apply method must be implemented');
  }

}
