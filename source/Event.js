import Message from './Message';

export default class Event extends Message {
  constructor() {
    super();
    this.version = 0;
  }
}
